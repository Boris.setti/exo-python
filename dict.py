#keys() ​​Retourne une vue sur les clés du dictionnaire.

#values()Retourne une vue sur les valeurs du dictionnaire.

#items()Retourne une vue sur les couples (clé, valeur) du dictionnaire.

#get(clé)Retourne la valeur associée à la clé spécifiée. Si la clé n'est pas présente dans le dictionnaire, retourne la valeur None  .

#pop(clé)Supprime la clé spécifiée et retourne la valeur associée. Si la clé n'est pas présente dans le dictionnaire, retourne la valeur None  .

#clear()Supprime tous les éléments du dictionnaire.


test={}

test["nom"]= "Staline"
test["prenom"]="Josephe"
test["nom"]= "sdrgs"

print(test.values())